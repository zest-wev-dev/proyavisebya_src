var 
    isBuild = false,
    gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    bs = require('browser-sync').create(),
    stream = bs.stream,
    reload = bs.reload,
    sass = require('gulp-sass'),
    notify = require("gulp-notify"),
    plumber = require("gulp-plumber"),
    prefixer = require("gulp-autoprefixer"),
    sourcemaps = require("gulp-sourcemaps"),
    pug = require("gulp-pug"),
    del = require("del"),
    rs = require("run-sequence"),
    concatFilenames = require('gulp-concat-filenames'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    rename = require("gulp-rename"),
    path = require("path"),
    args = require('yargs').argv,
    cleanCSS = require('gulp-clean-css'),
    minifyOpt = { ext: { min: '.js' }, noSource: true }
    ;


var pullAssets = args.pullAssets || false,
    copyToServer = args.cts || 'no',
    srcPath = '.',
    scssPath = srcPath + '/scss',
    blocksPath = srcPath + '/proayvi.lc/wp-content/plugins/zest-blocks',
    
    // assetsPathJs = assetsPath + '/js',
    // assetsPathImg = assetsPath + '/img',
    // assetsPathImgUi = srcPath +'/pug/ui/images',
    // siteRootPath = './site',
//    themePath = './site/wp-content/themes/zest-space-timber',
    themePath = './proayvi.lc/wp-content/themes/zest-web',
    themeAssetsPath = themePath+ '/assets';
// var assetsPathImages = './site/images';

gulp.task('clean', function (done) {
    return del(themeAssetsPath);
    done();
});

gulp.task('copy:fonts', function () {
    return gulp.src( srcPath +'/fonts/**/*.*')
        .pipe(gulp.dest( themeAssetsPath +'/fonts'));
});

gulp.task('scssConcat', function (done) {
    let rootPath = blocksPath +'/';
    let srcPath = rootPath +'/**/*.scss';
    let concatFilenamesOptions = {
        root: rootPath,
        prepend: "@import './../../plugins/zest-blocks/",
        append: "';"
    }
    gulp.src(srcPath)
        .pipe(concatFilenames('_autoimport.scss', concatFilenamesOptions))
        .pipe(gulp.dest(scssPath));

    done();
});
gulp.task('scssCompile', function (done) {
    if(isBuild){
        gulp.src( scssPath+'/style.scss')
        .pipe(sass())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest( themeAssetsPath+ '/css'));
    }
    else{
        gulp.src( scssPath+'/style.scss')
        .pipe(plumber({
            errorHandler: notify.onError(function (err) {
                return {
                    title: 'Styles',
                    message: err.message
                }
            })
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest( themeAssetsPath+ '/css'))
        .pipe(stream());
    }
    
    done();
});
gulp.task('scss', gulp.series('scssConcat', 'scssCompile' ) );

gulp.task('importJsBlocks', function (done) {
    if(isBuild){
        gulp.src(blocksPath+ '/**/*.js')
            .pipe(concat('blocks.js'))
            .pipe(minify( minifyOpt ))
            .pipe(gulp.dest(srcPath+ '/js/'));
    }
    else{
        gulp.src(blocksPath+ '/**/*.js')
            .pipe(concat('blocks.js'))
            .pipe(gulp.dest(srcPath+ '/js/'));
    }
    done();
});

//gulp.task('concatJs', function (done) {
//    if(isBuild){
//        gulp.src( srcPath +'/js/**/*.*')
//            .pipe(concat('main.js'))
//            .pipe(minify( minifyOpt ))
//            .pipe(gulp.dest( themeAssetsPath +'/js'));
//    }
//    else{
//        gulp.src( srcPath +'/js/**/*.*')
//            .pipe(concat('main.js'))
//            .pipe(gulp.dest( themeAssetsPath +'/js'));
//    }
//    done();
//});

gulp.task('copy:js', function (done) {
    
    if(isBuild){
        gulp.src( srcPath +'/js/**/*.*')
            .pipe(concat('main.js'))
            .pipe(minify( minifyOpt ))
            .pipe(gulp.dest( themeAssetsPath +'/js'));
    }
    else{
        gulp.src( srcPath +'/js/**/*.*')
            .pipe(concat('main.js'))
            .pipe(gulp.dest( themeAssetsPath +'/js'));
    }
    done();
    
    
//    if(isBuild){
//        gulp.src( srcPath +'/js/**/*.*')
//        .pipe(minify( minifyOpt ))
//        .pipe(gulp.dest( themeAssetsPath +'/js'));
//    }
//    else{
//        gulp.src( srcPath +'/js/**/*.*')
//        .pipe(gulp.dest( themeAssetsPath +'/js'));
//    }
//    done();
});
gulp.task('js', gulp.series('importJsBlocks', 'copy:js'));


gulp.task('copy:img', function (done) {
    gulp.src( srcPath +'/img/**/*.*')
        .pipe(gulp.dest(themeAssetsPath +'/img'));
    
    gulp.src( blocksPath +'/**/img/**/*.*')
        .pipe(gulp.dest(themeAssetsPath +'/img/blocks'));
    
    done();
});

gulp.task('copy:libs', function () {
    return gulp.src( srcPath +'/libs/**/*.*')
        .pipe(gulp.dest( themeAssetsPath +'/libs'));
});





    
gulp.task('serve', function(done) {
  connect.server({
    // port: 8000, //default
    // hostname: '127.0.0.1' //default
    base: './site',
    open: true
  });
    done();
});



// gulp.task('setIsBuild', function(done) {
//     isBuild = true;
//     done();
// });

// gulp.task('build', gulp.series('setIsBuild', 'scss', 'js', 'copy:fonts', 'copy:img', 'copy:libs', 'importLibs', 'importJsBlocks') );

gulp.task('watchers', function (done) {
    gulp.watch( blocksPath +'/**/*.scss', gulp.series('scssConcat') );
    gulp.watch( srcPath +'/**/*.scss', gulp.series('scssCompile') );
    gulp.watch( [blocksPath +'/**/*.js', srcPath +'/js/**/*.js'], gulp.series('js') );
    // gulp.watch( srcPath +'/img/**/*.*', gulp.series('copy:img') );
    // gulp.watch( srcPath +'/images/**/*.*', gulp.series('copy:img') );
    gulp.watch( srcPath +'/fonts/**/*.*', gulp.series('copy:fonts') );
    // gulp.watch( srcPath +'/libs/**/*.*', gulp.series('importLibs') );
    // gulp.watch( srcPath +'/pug/**/*.js', gulp.series('importJsBlocks') );
    done();
});

//gulp.task('default', ['connect']);

// gulp.task(
//     'server',
//     gulp.series(
//         gulp.parallel('scss', 'js', 'copy:fonts', 'copy:img', 'copy:libs', 'importLibs', 'importJsBlocks'),
// //        'serve',
//         'watchers'
//     )
// );
gulp.task(
    'server',
    gulp.series(
        gulp.parallel('scss', 'js', 'copy:fonts', 'copy:img', 'copy:libs'),
//        'serve',
        'watchers'
    )
);

gulp.task('setIsBuild', function(done) {
    isBuild = true;
    done();
});

gulp.task('build', gulp.series('setIsBuild', 'scss', 'js', 'copy:fonts', 'copy:img', 'copy:libs') );


gulp.task('default', gulp.series('clean', 'server'));
