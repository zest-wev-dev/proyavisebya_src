function Input(){
    
    this.mask = function(inp){
        if( !_.isElement(inp) ) return '';
        if( inp.classList.contains('is-phone') ) this.maskPhone(inp);
        else if( inp.classList.contains('is-email') ) this.maskEmail(inp);
        else if( inp.classList.contains('is-date') ) this.maskDate(inp);
        else if( inp.classList.contains('is-time') ) this.maskTime(inp);
        
    }
    this.maskPhone = function(inp){
        jQuery(inp).mask("+7 (999) 999-99-99");
    }
    this.maskEmail = function(inp){}
    this.maskDate = function(inp){}
    this.maskTime = function(inp){
        jQuery(inp).mask("99:99");
    }
    
    this.validate = function(inp){
        if( !_.isElement(inp) ) return '';
        var tag = inp.tagName;
        var type = inp.type;

        if(tag == 'SELECT') {
            var value = inp.options[inp.selectedIndex].value;
        }
        else if(type == 'checkbox'){
            if(inp.checked) var value = 'checked';
            else var value = '';
        }
        else var value = inp.value;

        var validationStatus = true;
        if( inp.classList.contains('is-required') ){
            if( !this.validateReqiured(value) ) validationStatus = false;
        }
        if( inp.classList.contains('is-email') ){
            if( !this.validateEmail(value) ) validationStatus = false;
        }
        if( inp.classList.contains('is-phone') ){
            if( !this.validatePhone(value) ) validationStatus = false;
        }
        if( inp.classList.contains('is-time') ){
            if( !this.validateTime(value) ) validationStatus = false;
        }
        if( inp.classList.contains('is-datetime') ){
            if( !isDateInputValid ) validationStatus = false;
        }
//        console.log('validationStatus', validationStatus);
        return validationStatus;
    }
    this.validatePhone = function(value){
        var re = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/gm;
        return re.test(value);
    }
    this.validateEmail = function(value){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(value.toLowerCase());
    }
    this.validateTime = function(value){
        var data = value.split(':');
        if( !_.isNumber( parseInt(data[0])) || !_.isNumber( parseInt(data[1])) ) return false;
        if( data[0] > 23 ) return false;
        if( data[1] > 59 ) return false;
        return true;
    }
    this.validateReqiured = function(value){
        return !_.isEmpty(value);
    }
    
}  

function validateForm(form){
    var validateInputs = form.querySelectorAll('.validate-it');
    var formValidationStatus = true;
    if(validateInputs.length>0){
        var _input = new Input();
        validateInputs.forEach(function(validateInput) {
            var tag = validateInput.tagName;
            var inputValidationStatus = _input.validate(validateInput);
            if(!inputValidationStatus){
                if(tag == 'SELECT'){
                    validateInput.closest('div').classList.add('has-error');
                }
                validateInput.classList.add('has-error');
                formValidationStatus = false;
            }
            else{
                if(tag == 'SELECT'){
                    validateInput.closest('div').classList.remove('has-error');
                }
                validateInput.classList.remove('has-error');
            }
        });
    }
    if(!formValidationStatus){
        var title = 'Обнаружены ошибки!';
        var text = 'Проверьте введеные данные';
        Swal.fire({
            icon: 'error',
            text, title
        });
    }
    return formValidationStatus;
}