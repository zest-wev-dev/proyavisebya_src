jQuery(function ($) {
  $("a[href*='#']:not([href='#'])").click(function (e) {
    e.preventDefault();
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top,
          },
          1000
        );
        // return false;
      }
    }
  });
});
//   $(window).on("scroll", function (e) {
//     var top = $(window).scrollTop();
//     if (top > 500 && !$header.is(".is-dark")) {
//       $header.addClass("is-dark");
//     }
//     if (top < 500 && $header.is(".is-dark")) {
//       $header.removeClass("is-dark");
//     }

//     console.log("$(window).scrollTop()");
//   });
// });

// console.log("window.innerWidth");

// window.addEventListener("preloaderOut", function (e) {
//   if (window.innerWidth < 1100) return;

