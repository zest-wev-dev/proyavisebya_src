jQuery(function($){
    // var $form = $('');
    var dev = false;
    var widget = false;



    $('body').on('submit', '.js-order-form', function(e){
        e.preventDefault();
        var form = this;
        var status = validateForm(form);
        if( !status ){
            return;
        }

        $(form).addClass('is-loading');
        ajaxs( 'order', $(form), function(response){
            console.log('response', response);
            var data = response.data;
            console.log('data', data);
            if (dev){
                $(form).removeClass('is-loading');
                return;
            } 
            if(response.data.status == 'success'){
                Swal.fire({
                    icon: 'success',
                    title: 'Спасибо за обращение!',
                    text: 'Ваша заявка отправлена!',
                    footer: '<span>Идет перенеправление на страницу оплаты</span>'
                });
                
                if(widget){
                    showPaymentForm(response.data);
                    $(form).find('.ui-form__row_submit').addClass('hidden');
                    $('.js-payment-form').removeClass('hidden');
                    $('.js-paydata-btn').trigger('click');
                }
                else{
                    _.delay(function(){
                        if(data.payment_url){
                            window.location = data.payment_url;
                        }
                    }, 1500);
                }
                
                
                // if(response.data.price>0){
                //     showPaymentForm(response.data);
                //     $(form).find('.ui-form__row_submit').addClass('hidden');
                //     $('.js-payment-form').removeClass('hidden');
                //     $('.js-paydata-btn').trigger('click');
                // }
            }
            else{
                // var text = 'При отправке вашей заявки произошла ошибка!';
                Swal.fire({
                    icon: 'error',
                    // title: 'Спасибо за обращение!',
                    text: 'При отправке вашей заявки произошла ошибка!'
                });
            }
            // jQuery.notiny({ text });       
            $(form).removeClass('is-loading');
        });
    });

    // function validateForm($form){
    //     return true;
    // }
    function showPaymentForm(data){
        // console.log('форма оплаты');
        // console.log('data', data);
        $('.js-paydata-amount').val( data['price'] );
        $('.js-paydata-order').val( data['post_id'] );
        $('.js-paydata-name').val( data['fio'] );
        $('.js-paydata-email').val( data['email'] );
        $('.js-paydata-phone').val( data['phone'] );
        $('.js-paydata-phone').val( data['phone'] );
        $('.js-paydata-description').val( data['tarif'] );
    }


    if( orderPaymentStatus == 'yes'){
        Swal.fire(
            'Спасибо!',
            'Ваш заказ оплачен!',
            'success'
        );
    }
    if( orderPaymentStatus == 'error'){
        Swal.fire(
            'Что-то пошло не так!',
            'При обработке платежа произошла ошибка!',
            'error'
        );
    }
});