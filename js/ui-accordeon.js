var $accordeonItems = document.querySelectorAll('.ui-accordeon-item');

$accordeonItems.forEach(function($item){
    if($item.classList.contains('is-active')) showAccordeonBody($item);
    var $head = $item.querySelector('.ui-accordeon-item__head');
    
    $head.addEventListener('click', function(e){
        var $this = e.currentTarget;
        var $item = $this.closest('.ui-accordeon-item');

        if($item.classList.contains('is-active')){
            hideAccordeonItem($item);
            return;
        }

        var $accordeon = $item.closest('.ui-accordeon');
        var $accordeonItems = $accordeon.querySelectorAll('.ui-accordeon-item');
        hideAccordeon($accordeonItems);
        showAccordeonBody($item);
    });
});

function showAccordeonBody($item){
    var $body = $item.querySelector('.ui-accordeon-item__body');
    var $bodyInner = $item.querySelector('.ui-accordeon-item__body-inner');
    $body.style.height = $bodyInner.offsetHeight +'px';
    $item.classList.add('is-active');
}
function hideAccordeon($items){
    $items.forEach(function($item){
        hideAccordeonItem($item)
    });
}
function hideAccordeonItem($item){
    var $body = $item.querySelector('.ui-accordeon-item__body');
    $body.style.height = '0px';
    $item.classList.remove('is-active');
}
