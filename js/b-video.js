var $videoBtns = document.querySelectorAll('.js-show-video');
if($videoBtns.length>0){
    $videoBtns.forEach(function($btn){
        $btn.addEventListener('click', function(e){
            e.preventDefault();
            var $this = e.currentTarget;
            var $item = $this.closest('.ui-video');
            var $frame = $item.querySelector('.ui-video__frame');
            var videoOpts = '?autoplay=1' + '&modestbranding=0' + '&controls=1' + '&showinfo=0' +'&rel=0';
            var video = 'https://www.youtube.com/embed/'+ $this.dataset.src + videoOpts;
            
            $frame.setAttribute('src', video);
            $frame.classList.add('is-active');
        });
    });
}
